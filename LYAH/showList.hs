showList :: (Show a) => [a] -> String  
showList [] = ""
showList [x] = show x
showList (x:xs) = show x ++ " " ++ Main.showList (xs)
