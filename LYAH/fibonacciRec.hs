fibonacci :: (Integral a) => a -> a
fibonacci x
    | x < 0 = error "Has to be a natural number!"
    | otherwise =  case x of
        0 -> 0
        1 -> 1
        x -> fibonacci (x-1) + fibonacci (x-2)
