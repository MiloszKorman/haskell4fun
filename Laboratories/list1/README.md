### Zad 1.
Napisz funkcję, która przyjmuje na wejściu listę liczb rzeczywistych, a następnie zwraca sumę tych liczb.

### Zad 2.
Napisz funkcję, która przejmuje dwa parametry: napis (będący separatorem) oraz lista napisów. Funkcja ta ma zwracać pojedynczy napis składający się z napisów tablicy oddzielonych separatorem.

### Zad 3.
Napisz funkcję, która przyjmuje dwa parametry: listę liczb całkowitych i liczbę całkowitą (n). Ma ona zwracać tablicę tylko z liczbami większymi niż zadana liczba n, które znajdowały się w tablicy będącej pierwszym parametrem.

### Zad 4.
Napisz funkcję przyjmującą 3 parametry. Funkcja ma zwracać wartość boolowska (true/false) w zależności od tego czy liczby są ustawione malejąco czy też nie.

### Zad 5.
Napisz funkcję przyjmującą znak i listę znaków i liczącą liczbę wystąpień tego znaku
