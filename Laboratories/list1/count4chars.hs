count4chars :: Char -> [Char] -> Integer
count4chars el = sum . map (\x -> 1) . filter (==el)
