sumRel :: (Floating a) => [a] -> a
sumRel = foldl (\acc x -> acc + x) 0.0
