stringJoiner :: [Char] -> [[Char]] -> [Char]
stringJoiner _ [] = []
stringJoiner _ [x] = x
stringJoiner sep (x:xs) = x++sep++stringJoiner sep xs
