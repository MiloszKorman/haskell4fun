areDescending :: (Ord a) => a -> a -> a -> Bool
areDescending x y z = (x > y) && (y > z)
