## Zad 1.
Zdefiniuj funkcję filtrującą elementy z listy list. Funkcja ma mieć dwa parametry: listę list i wartość.
W liście wynikowej pozostać mają tylko te elementy, których każdy element listy jest różny od
podanej wartości.
### Przykład: [[1;2;3];[3;4];[5;6]] 3 -> [[5;6]]

## Zad 2.
Zdefiniuj funkcję przekształcającą listę cyfr binarnych [0;1] w liczbę dziesiętną. Funkcja ma mieć jeden
parametr.
### Przykład [1;0;1] -> 5
