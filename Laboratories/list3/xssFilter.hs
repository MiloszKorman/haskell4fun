xssFilter :: (Eq a) => [[a]] -> a -> [a]
xssFilter [] _ = []
xssFilter (xs:xss) x
  | elem x xs = xssFilter xss x
  | otherwise = xs++xssFilter xss x
