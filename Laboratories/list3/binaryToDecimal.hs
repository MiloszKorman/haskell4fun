binaryToDecimal :: [Int] -> Int
binaryToDecimal =
  fst . foldr (\x (res, wei) -> (res + x*wei, wei*2)) (0, 1)
