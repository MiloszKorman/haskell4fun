segregator :: [Int] -> ([Int], [Int], [Int])
segregator xs = innerSegregator xs ([], [], [])
  where
    innerSegregator :: [Int] -> ([Int], [Int], [Int]) -> ([Int], [Int], [Int])
    innerSegregator [] res = res
    innerSegregator (x:xs) (e, po, no) 
      | (even x) = innerSegregator xs (x:e, po, no)
      | x >= 0 = innerSegregator xs (e, x:po, no)
      | otherwise = innerSegregator xs (e, po, x:no)
