import Data.Bits

findOddOne :: [Int] -> Int
findOddOne [] = 0
findOddOne (x:xs) = x `xor` findOddOne xs
