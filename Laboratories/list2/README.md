## Zad 1.
Napisz funkcję rozdzielającą listy wejściowe na 3 podlisty. W pierwszej podliście mają się znaleźć
wszystkie elementy parzyste. W drugiej podliście mają się znaleźć wszystkie elementy o wartościach
nieparzystych dodatnich. W trzeciej nieparzyste ujemne.
### Porządek elementów musi być zachowany. Wynik zwrócić w postaci pary list.

## Zad 2.
Napisz funkcję łączącą dwie podane listy. Elementy w liście wyjściowej mają występować
naprzemiennie.

## Zad 3.
Dostajesz dużą listę liczb, każda liczba występuje w tej liście parzystą liczbę razy, znajdź jedyną liczbę, która występuje nieparzystą liczbę razy

