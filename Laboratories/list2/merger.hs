merger :: [a] -> [a] -> [a]
merger [] [] = []
merger xs [] = xs
merger [] ys = ys
merger (x:xs) (y:ys) = x:y:merger xs ys
