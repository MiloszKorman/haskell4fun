flatten :: [[a]] -> [a]
flatten [] = []
flatten [xs] = xs
flatten (xs:xxs) = xs ++ flatten xxs
