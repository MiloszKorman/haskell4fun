sqrList :: (Num a) => [a] -> [a]
sqrList [] = []
sqrList (x:xs) = (x^2):sqrList xs
