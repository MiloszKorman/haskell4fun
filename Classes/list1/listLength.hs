listLength :: [a] -> Integer
listLength = sum . map (\x -> 1)
