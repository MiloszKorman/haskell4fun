replicate' :: a -> Integer -> [a]
replicate' _ 0 = []
replicate' x n = x : replicate' x (n-1)
