count :: (Eq a) => a -> [a] -> Integer
count el = sum . map (\x -> 1) . filter (==el)
