palindrome :: (Eq a) => [a] -> Bool
palindrome [] = True
palindrome [x] = True
palindrome (xh:xs)
  | xh == (last xs) = palindrome (init xs)
  | otherwise = False
