# Paradygmaty programowania - ćwiczenia

# Lista 1

1. Zdefiniuj funkcję flatten, która dla argumentu będącego listą list
tworzy listę, złożoną z elementów wszystkich podlist z zachowaniem ich kolejności,
np. flatten [[5;6];[1;2;3]] zwraca [5; 6; 1; 2; 3], czyli spłaszcza listę o jeden poziom.

2. Zdefiniuj funkcję count obliczającą ile razy dany obiekt występuje
w danej liście, np. count ('a', ['a'; 'l'; 'a']) zwraca 2.

3. Zdefiniuj funkcję replicatepowtarzającą dany obiekt określoną liczbę
razy i zwracającą wynik w postaci listy, np. replicate ("la",3) zwraca ["la"; "la"; "la"].

4. Zdefiniuj funkcję sqrList podnoszącą do kwadratu wszystkie elementy
danej listy liczb, np. sqrList [1;2;3;-4] zwraca [1; 4; 9; 16].

5. Zdefiniuj funkcję palindrome sprawdzającą, czy dana lista jest
palindromem, tj. równa się sobie samej przy odwróconej kolejności elementów,
np. palindrome ['a'; 'l'; 'a'] zwraca true.

6. Zdefiniuj swoją funkcję listLength, obliczającą długość dowolnej listy
(oczywiście bez użycia standardowej funkcji List.length).

