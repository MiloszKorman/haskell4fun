# Paradygmaty programowania - ćwiczenia

# Lista 3

1. Zdefiniuj funkcje a) curry3 i b) uncurry3, przeprowadzające konwersję między zwiniętymi
i rozwiniętymi postaciami funkcji od trzech argumentów. Podaj ich typy.

2. Przekształć poniższą rekurencyjną definicję funkcji sumProd, która oblicza jednocześnie
sumę i iloczyn listy liczb całkowitych na równoważną definicję nierekurencyjną z
jednokrotnym użyciem funkcji bibliotecznej foldl, której argumentem jest
odpowiednia funkcja anonimowa (literał funkcyjny).
Kod w Ocamlu:
let rec sumProd xs =
  match xs with
      h::t -> let (s,p)= sumProd t in (h+s,h*p)
    | [] -> (0,1);;

3. Zdefiniuj funkcje sortowania
a) przez wstawianie z zachowaniem stabilności i złożoności O(n^2 ) insertionsort
b) przez łączenie (scalanie) z zachowaniem stabilności i złożoności O(n lg n) mergesort

## Uwaga! Przypominam, że funkcje List.append i List.length mają złożoność liniową!
