insertSort :: (Ord a) => [a] -> [a]
insertSort = foldl (\acc x -> addProperPlace x acc) []
  where
    addProperPlace :: (Ord a) => a -> [a] -> [a]
    addProperPlace el [] = [el]
    addProperPlace el (x:xs) =
      if el <= x then el:x:xs
        else x:addProperPlace el xs
