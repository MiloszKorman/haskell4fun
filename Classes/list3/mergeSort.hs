mergeSort :: (Ord a) => [a] -> [a]
mergeSort [] = []
mergeSort [x] = [x]
mergeSort xs = (mergeSort $ firstHalf xs) `connect` (mergeSort $ secondHalf xs)
  where
    firstHalf xs = take ((length xs) `div` 2) xs
    secondHalf xs = drop ((length xs) `div` 2) xs
    connect :: (Ord a) => [a] -> [a] -> [a]
    connect [] [] = []
    connect xs [] = xs
    connect [] ys = ys
    connect fxs@(x:xs) fys@(y:ys)
      | x <= y = x:connect xs fys
      | otherwise = y:connect fxs ys
