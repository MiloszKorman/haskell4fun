replaceNth :: [a] -> Int -> a -> [a]
replaceNth [] _ _ = []
replaceNth (x:xs) 0 el = el:xs
replaceNth (x:xs) n el = x:replaceNth xs (n-1) el
