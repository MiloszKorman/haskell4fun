fibonacci :: Int -> Int
fibonacci 0 = 0
fibonacci n = snd $ foldl (\(ll, l) _ -> (l, ll + l)) (0, 1) [2..n]
