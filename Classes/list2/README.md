# Paradygmaty programowania - ćwiczenia

# Lista 2

## 1.
Liczby Fibonacciego są zdefiniowane następująco:
f(0) = 0
f(1) = 1
f(n+2) = f(n) + f(n+1)
Napisz funkcję, która dla danego n znajdują n-tą liczbę Fibonacciego wykorzystującą rekursję ogonową.

Dokładność jest osiągnięta, jeśli |xi^3 – a| <= E * |a|. Napisz efektywną (wykorzystującą rekursję ogonową) funkcję root3, która dla zadanej liczby a znajduje pierwiastek trzeciego stopnia z dokładnością 10-^15.

## 2.
Zdefiniuj funkcję initSegment sprawdzającą w czasie liniowym, czy pierwsza lista stanowi początkowy segment drugiej listy. Każda lista jest swoim początkowym segmentem, lista pusta jest początkowym segmentem każdej listy.

## 3.
Zdefiniuj funkcję replaceNth, zastępującą n-ty element listy podaną wartością (pierwszy element ma numer 0), np. replaceNth (['o'; 'l'; 'a'] ,1, 's') => ['o'; 's'; 'a']
