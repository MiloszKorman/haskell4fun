initSegment :: (Eq a) => [a] -> [a] -> Bool
initSegment [] _ = True
initSegment _ [] = False
initSegment (x:xs) (y:ys)
  | x == y = initSegment xs ys
  | otherwise = False
